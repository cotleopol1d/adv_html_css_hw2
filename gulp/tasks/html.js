import fileInclude from "gulp-file-include";
export const html = (done) => {
    app.gulp.src(app.path.src.html)
       .pipe(app.plugins.plumber(app.plugins.notify.onError({
           title: "HTML",
           massage: "Error: <%= error.message %>"
       })))
        .pipe(fileInclude())
        .pipe(app.gulp.dest(app.path.build.html))
       .pipe(app.plugins.browserSync.stream());
    done();
}