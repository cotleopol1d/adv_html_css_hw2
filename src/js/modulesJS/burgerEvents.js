function burgerEvents(object) {
    object.burger.classList.toggle("button-burger--active");
    object.menu.classList.toggle("nav-menu--mobile");
}